<?php

error_reporting(E_ALL);
ini_set('display_errors', '1');

require_once("model.php");

// Maps actions to the functions that will carry them out.
$ACTION_MAP = array(
    "signup"        => "signup", 
    "login"         => "login", 
    "add_settings"  => "addSettings", 
    "save_settings" => "saveSettings",
    "get_settings"  => "getSettings",
    "get_one_setting" => "getOneSetting",
    "logout"        => "logout"
);

// Figure out current user status.
$loggedInUser = null;
if(array_key_exists("TODOID", $_COOKIE)){
    if($_COOKIE["TODOID"] != ""){
        $user = getUserInfoByAuthToken($_COOKIE["TODOID"]);
        if($user != null){
            $loggedInUser = $user["id"];
        }
    }
}

// Call the correct action.
if(array_key_exists("action", $_POST) && 
        array_key_exists($_POST["action"], $ACTION_MAP)){
    $ACTION_MAP[$_POST["action"]]($_POST);
} elseif($loggedInUser == null){
    redirectToLogin();
}

if(!array_key_exists("action", $_POST) || $_POST["action"] == "login")
    // Load the todo list.
    require_once("visualizer.php");
else
    print json_encode(["success" => true]);
    // print "success";



// FUNCTIONS

/**
 * Signs the user up. Requires that $data has the following keys:
 *  - username
 *  - password
 */
function signup($data){
    logout($data, false);

    // Make sure their passwords match.
    if($data["password"] != $data["password2"]){
        redirectToSignup("Make sure your passwords match.");
    }

    // Make sure the username is okay.
    $user = getUserInfo($data["username"]);
    if($user != null){
        redirectToSignup("That username is taken. Try a different one.");
    }

    // Add the user.
    addNewUser($data["username"], 
        password_hash($data["password"], PASSWORD_BCRYPT));

    // Redirect them to the login page.
    redirectToLogin("", "Account created! Now log in.");
}

/**
 * Logs the user in. Requires that $data has the following keys:
 *  - username
 *  - password
 */
function login($data){
    global $loggedInUser;
    logout($data, false);

    // Authenticate the user.
    $user = getUserInfo($data["username"]);
    if(!password_verify($data["password"], $user["password"])){
        redirectToLogin("Invalid username or password.");
    }

    // Generate an authentication token.
    $loggedInUser = $user["id"];
    $authToken = bin2hex(random_bytes(25));
    setUserAuthToken($loggedInUser, $authToken);
    if(array_key_exists("remember-me", $data))
        setcookie("TODOID", $authToken, time()+60*60*24*14); // 2 weeks.
    else
        setcookie("TODOID", $authToken);
}

/**
 * Logs the user out.
 */
function logout($data, $redirect=true){
    global $loggedInUser;

    if($loggedInUser != null){
        setUserAuthToken($loggedInUser, "");
        setcookie("TODOID", "", time() - 3600);
    }

    if($redirect)
        redirectToLogin();
}

/**
 * Adds a new todo item for the currently logged in user. Requires that $data 
 * has the following keys:
 *  - description
 */
function addSettings($data){
    global $loggedInUser;
    bootUserIfNotLoggedIn();
    addSettingsToDB($loggedInUser, $data["settings_id"], $data["gravity"], $data["link_distance"], $data["charge"], $data["node_size"], $data["label_size"]);
}

function saveSettings($data) {
    global $loggedInUser;
    bootUserIfNotLoggedIn();
    saveSettingsToDB($loggedInUser, $data["settings_id"], $data["gravity"], $data["link_distance"], $data["charge"], $data["node_size"], $data["label_size"]);
}

function getSettings($data) {
    global $loggedInUser;
    bootUserIfNotLoggedIn();
    getSettingsFromDB($loggedInUser);
}

function getOneSetting($data) {
    global $loggedInUser;
    bootUserIfNotLoggedIn();
    getOneSettingFromDB($loggedInUser, $data["settings_id"]);
}

/**
 * If no user is logged in, redirects to the login page.
 */
function bootUserIfNotLoggedIn(){
    global $loggedInUser;
    if($loggedInUser == null){
        redirectToLogin();
    }
}

/**
 * Redirects to the login page, adding in provided errors or messages.
 */
function redirectToLogin($error="", $message="") {
    echo '
    <!DOCTYPE html>
    <html>
    <head>
        <meta http-equiv="refresh" content="0; url=login.html?error='. 
        rawurlencode($error) .'&message='. rawurlencode($message) .'" />
    </head>
    </html>
    <html>
    ';
    exit();
}

/**
 * Redirects to the signup page, adding in provided errors or messages.
 */
function redirectToSignup($error="", $message="") {
    echo '
    <!DOCTYPE html>
    <html>
    <head>
        <meta http-equiv="refresh" content="0; url=signup.html?error='. 
        rawurlencode($error) .'&message='. rawurlencode($message) .'" />
    </head>
    </html>
    <html>
    ';
    exit();
}


?>


