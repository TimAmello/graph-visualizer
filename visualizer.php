<!DOCTYPE html>
<meta charset="utf-8">
<html>

<head>
    <title>Graph Visualizer</title>
    <!-- JQUERY -- permission granted by administrator -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.js"></script>
    <!-- D3JS -- permission granted by admininstrator -->
    <script src="https://d3js.org/d3.v3.min.js" charset="utf-8"></script>
    <!-- Papa Parse -- permission granted by administrator -->
    <script src="https://fastcdn.org/Papa-Parse/4.1.2/papaparse.min.js"></script>
    <!-- Save SVG as PNG -- permission granted by administrator -->
    <script src="savesvgaspng.js"></script>
    <!-- Bootstrap -- permission granted by administrator -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <link rel="stylesheet" type="text/css" href="style.css">


    <script type="text/javascript">
        var fileext = "";
        // https://stackoverflow.com/a/7977314
        // basic file extension check
        function isCorrectFile(filename) {
            var parts = filename.split('.');
            fileext = parts[parts.length - 1];
            switch (fileext.toLowerCase()) {
                case 'csv':
                case 'tsv':
                    return true;
            }
            return false;
        }

        var chosenSettings = '';

        var fillSelectors = function(event) {

            $('select').empty();

            Papa.parse(document.getElementById('fileToUpload').files[0], {
                header: true,
                complete: function(results) {
                    console.log(results.meta['fields']);
                    var sourceSelector = document.getElementById('source-selector');
                    var targetSelector = document.getElementById('target-selector');
                    for (var i = 0; i < results.meta['fields'].length; i++) {
                        var opt = results.meta['fields'][i];
                        var el = document.createElement("option");
                        el.textContent = opt;
                        el.value = opt;
                        var elTwo = el.cloneNode(true);
                        sourceSelector.appendChild(el);
                        targetSelector.appendChild(elTwo);
                    }
                }
            });
        }

        var newFileUploaded = function(event) {


            var file = URL.createObjectURL(document.getElementById('fileToUpload').files[0]);
            var filename = document.getElementById('fileToUpload').files[0].name;



            if (isCorrectFile(filename)) {
                $('#incorrectFileTypeTag').css('display', 'none');


                var width = 960;
                var height = 500;
                var links = [];

                if (fileext == 'csv') {
                    d3.csv(file, function(data) {
                        data.forEach(function(d) {
                            links.push({
                                source: d[$('#source-selector :selected').text()],
                                target: d[$('#target-selector :selected').text()]
                            });
                        })
                        doSomething();
                    });
                }
                if (fileext == 'tsv') {
                    d3.tsv(file, function(data) {
                        data.forEach(function(d) {
                            links.push({
                                source: d[$('#source-selector :selected').text()],
                                target: d[$('#target-selector :selected').text()]
                            });
                        })
                        doSomething();
                    });
                }




                function doSomething() {

                Papa.parse(document.getElementById('fileToUpload').files[0], {
                        header: true,
                        complete: function(results) {
                            $('#edgeTable tbody').empty();
                            var html = '';
                            console.log(results.data[1][$('#source-selector :selected').text()]);
                            for (var i = 0; i < results.data.length; i++) {
                                html += '<tr><td>' + results.data[i][$('#source-selector :selected').text()] + '</td><td>' + results.data[i][$('#target-selector :selected').text()] + '</td></tr>';
                            }
                            $('#edgeTable tbody').append(html);
                            $('edgeTable').css("display", "block");
                        }
                    });
                
                if (!$('#stickyToggle').is(":checked")) {
                    var nodesByName = {};

                    /*

                    CORE GRAPHING CODE TAKEN FROM bl.ocks.org/mbostock/2949937
                    PERMITTED UNDER GNU General Public License Version 3

                    */

                    // Create nodes for each unique source and target.
                    links.forEach(function(link) {
                        link.source = nodeByName(link.source);
                        link.target = nodeByName(link.target);
                    });

                    var width = 960,
                        height = 500;

                    $("#graph").remove();
                    var svg = d3.select("#tableAndGraph").append("svg")
                        .attr("id", "graph")
                        .attr("width", width)
                        .attr("height", height)
                        .style("display", "inline-block");

                    var force = d3.layout.force()
                        .size([width, height]);


                    // Extract the array of nodes from the map by name.
                    var nodes = d3.values(nodesByName);

                    // Create the link lines.
                    var link = svg.selectAll(".link")
                        .data(links)
                        .enter().append("line")
                        .attr("class", "link");

                    // Create the node circles.
                    var node = svg.selectAll(".node")
                        .data(nodes)
                        .enter().append("circle")
                        .attr("class", "node")
                        .attr("r", $('#nodeSize').val())
                        .call(force.drag);

                    var label = svg.selectAll(null)
                        .data(nodes)
                        .enter()
                        .append("text")
                        .text(function (d) { return d.name; })
                        .style("text-anchor", "middle")
                        .style("fill", "#555")
                        .style("font-family", "Arial")
                        .style("font-size", $('#labelSize').val());

                    // Start the force layout.
                    force
                        .nodes(nodes)
                        .links(links)
                        .charge(-1 * $("#charge").val())
                        .gravity($("#gravity").val())
                        .linkDistance($("#linkDistance").val())
                        .on("tick", tick)
                        .start();

                    function tick() {
                        link.attr("x1", function(d) {
                                return d.source.x;
                            })
                            .attr("y1", function(d) {
                                return d.source.y;
                            })
                            .attr("x2", function(d) {
                                return d.target.x;
                            })
                            .attr("y2", function(d) {
                                return d.target.y;
                            });

                        node.attr("cx", function(d) {
                                return d.x;
                            })
                            .attr("cy", function(d) {
                                return d.y;
                            });

                        label.attr("x", function(d) {
                                return d.x;
                            })
                            .attr("y", function(d) {
                                return d.y-17;
                            });
                    }

                    function nodeByName(name) {
                        return nodesByName[name] || (nodesByName[name] = {
                            name: name
                        });
                    }
                }
                else {
                    var nodesByName = {};

                    /*

                    CORE GRAPHING CODE TAKEN FROM bl.ocks.org/mbostock/2949937
                    PERMITTED UNDER GNU General Public License Version 3

                    */

                    // Create nodes for each unique source and target.
                    links.forEach(function(link) {
                        link.source = nodeByName(link.source);
                        link.target = nodeByName(link.target);
                    });

                    var width = 960,
                        height = 500;

                    $("#graph").remove();
                    var svg = d3.select("#tableAndGraph").append("svg")
                        .attr("id", "graph")
                        .attr("width", width)
                        .attr("height", height)
                        .style("display", "inline-block");

                    var force = d3.layout.force()
                        .size([width, height]);

                    var drag = force.drag()
                        .on("dragstart", dragstart);


                    // Extract the array of nodes from the map by name.
                    var nodes = d3.values(nodesByName);

                    // Create the link lines.
                    var link = svg.selectAll(".link")
                        .data(links)
                        .enter().append("line")
                        .attr("class", "link");

                    // Create the node circles.
                    var node = svg.selectAll(".node")
                        .data(nodes)
                        .enter().append("circle")
                        .attr("class", "node")
                        .attr("r", $('#nodeSize').val())
                        .on("dblclick", dblclick)
                        .call(force.drag);

                    var label = svg.selectAll(null)
                        .data(nodes)
                        .enter()
                        .append("text")
                        .text(function (d) { return d.name; })
                        .style("text-anchor", "middle")
                        .style("fill", "#555")
                        .style("font-family", "Arial")
                        .style("font-size", $('#labelSize').val());

                    // Start the force layout.
                    force
                        .nodes(nodes)
                        .links(links)
                        .charge(-1 * $("#charge").val())
                        .gravity($("#gravity").val())
                        .linkDistance($("#linkDistance").val())
                        .on("tick", tick)
                        .start();

                    function tick() {
                        link.attr("x1", function(d) {
                                return d.source.x;
                            })
                            .attr("y1", function(d) {
                                return d.source.y;
                            })
                            .attr("x2", function(d) {
                                return d.target.x;
                            })
                            .attr("y2", function(d) {
                                return d.target.y;
                            });

                        node.attr("cx", function(d) {
                                return d.x;
                            })
                            .attr("cy", function(d) {
                                return d.y;
                            });

                        label.attr("x", function(d) {
                                return d.x;
                            })
                            .attr("y", function(d) {
                                return d.y-17;
                            });
                    }

                    function dblclick(d) {
                        d3.select(this).classed("fixed", d.fixed = false);
                    }

                    function dragstart(d) {
                        d3.select(this).classed("fixed", d.fixed = true);
                    }

                    function nodeByName(name) {
                        return nodesByName[name] || (nodesByName[name] = {
                            name: name
                        });
                    }
                }


            }

        } else {
            $('#incorrectFileTypeTag').css('display', 'inline-block');
        }

        }

        function saveToPNG() {
          // https://stackoverflow.com/questions/4565112/javascript-how-to-find-out-if-the-user-browser-is-chrome/13348618#13348618
          var isChromium = window.chrome;
          var winNav = window.navigator;
          var vendorName = winNav.vendor;
          var isOpera = typeof window.opr !== "undefined";
          var isIEedge = winNav.userAgent.indexOf("Edge") > -1;
          var isIOSChrome = winNav.userAgent.match("CriOS");

          if (isIOSChrome) {
             alert("Sorry, this only works on the desktop version.");
          } else if(
              isChromium !== null &&
              typeof isChromium !== "undefined" &&
              vendorName === "Google Inc." &&
              isOpera === false &&
              isIEedge === false
          ) {
             if (document.getElementById("graph").hasChildNodes()) { 
                saveSvgAsPng(document.getElementById("graph"), "graph.png", {scale: 0.5});
            } else {
                alert("You haven't loaded the graph yet.");
            }
          } else { 
             alert("Sorry, this feature only works in Google Chrome.");
          }

        }

        function loadSettings() {
            $('#loadSettingsSelector').modal('show');
        }

        function saveSettings() {
            var name = $("#saveSettingsName").val();
            var gravityNum = parseFloat($('#gravity').val());
            var linkDistance = parseInt($('#linkDistance').val());
            var chargeNum = parseInt($('#charge').val());
            var nodeSize = parseInt($('#nodeSize').val());
            var labelSize = parseInt($('#labelSize').val());

            $.ajax({
                url: 'vis-handler.php',
                method: 'post',
                data: {
                    action: 'add_settings',
                    settings_id: name,
                    gravity: gravityNum,
                    link_distance: linkDistance,
                    charge: chargeNum,
                    node_size: nodeSize,
                    label_size: labelSize
                },
                success: (response)=>{
                    console.log(response);
                    alert("Saved!");
                    $("#saveSettingsMenu").modal('hide');
                },
                error: (xhr, status, error)=>{
                    alert(error);
                }
            });
            // Send some ajax.
        }

        function openSaveModal() {
            $("#loadSettingsSelector").modal('hide');
            $("#saveSettingsMenu").modal('show');
        }

        function openLoadModal() {
            $("#saveSettingsMenu").modal('hide');
            $('#loadSettingsSelector').modal('show');

            $.ajax({
                url: 'vis-handler.php',
                method: 'post',
                data: {
                    action: 'get_settings'
                },            
            success: function ($data) {
                console.log($data);
            },
            error: (xhr, status, error)=> {
                alert(error);
            }
        });
    }

    function selectSettings(event) {
        $(this).addClass("active").siblings().removeClass("active");
        chosenSettings = $(this).text();
        console.log(chosenSettings);
    }
    
    function loadSettings(event) {
            $('#loadSettingsSelector').modal('hide');

            $.ajax({
                url: 'vis-handler.php',
                method: 'post',
                data: {
                    action: 'get_one_setting',
                    settings_id: chosenSettings
                },
                success: function (data) {
                    var realData = data.replace('{"success":true}', '');
                    var eachSetting = realData.split(' ');

                    $('#gravity').val(eachSetting[0]);
                    $('#linkDistance').val(eachSetting[1]);
                    $('#charge').val(eachSetting[2]);
                    $('#nodeSize').val(eachSetting[3]);
                    $('#labelSize').val(eachSetting[4]);
                },
                error: (xhr, status, error)=> {
                    alert(error);
                } 
            });
    }


        $(document).ready(function() {
            // clear input field because FireFox holds onto it after refresh
            var inputs = $("#fileToUpload");
            try{
                inputs[0].value = '';
                if(inputs[0].value){
                    inputs[0].type = "text";
                    inputs[0].type = "file";
                }
            }catch(e){}

            $('#loadSettingsSelector').modal('hide');
            $('#saveSettingsMenu').modal('hide');

            $(document).on('click', '#newFileSubmit', newFileUploaded);
            $(document).on('change', '#fileToUpload', fillSelectors);
            $(document).on('click', '#saveToPNG', saveToPNG);
            $(document).on('click', '#openLoadModal', openLoadModal);
            $(document).on('click', '#openSaveModal', openSaveModal);
            $(document).on('click', '#saveSettingsButton', saveSettings);
            $(document).on('click', '#singleSettingBtn', selectSettings);
            $(document).on('click', '#loadSettingsButton', loadSettings);
        });
    </script>

</head>

<body>
    <h1>Graph Visualizer</h1>

    <div id="uploadBox">
        Upload a csv or tsv file:
        <input type="file" name="fileToUpload" id="fileToUpload">
        <button type="submit" id="newFileSubmit" name="submitNew" aria-label="Update">Update</button>
        <p id="incorrectFileTypeTag" style="display: none">That's not a .csv or .tsc file.</p>
    </div>



    <div id="tableAndGraph">
        <table id="edgeTable">
            <thead>
                <tr id="edgeTableHeadings">
                    <th>Source</th>
                    <th>Target</th>
                </tr>
            </thead>
            <tbody>
              
            </tbody>
        </table>
        <svg style="float: right;" id="graph"></svg>
    </div>


   <select id="source-selector">
       <option>n/a</option>
   </select>
   <select id="target-selector">
       <option>n/a</option>
   </select>

    <button type="submit" id="saveToPNG">Save to PNG</button>
    <button type="button" id="openLoadModal">Load Settings</button>
    <button type="button" id="openSaveModal">Save Settings</button>

    <label for="gravity">Gravity</label>
    <input type="number" id="gravity" name="gravity" min="0.05" max="1" step="0.05" value="0.05">

    <label for="linkDistance">Link Distance</label>
    <input type="number" id="linkDistance" name="linkDistance" min="0" max="300" step="10" value="50">
    
    <label for="charge">Charge</label>
    <input type="number" id="charge" name="charge" min="10" max="300" step="10" value="80">

    <label for="nodeSize">Node Size</label>
    <input type="number" id="nodeSize" name="nodeSize" min="2" max="30" step="0.5" value="5">
    
    <label for="labelSize">Label Size</label>
    <input type="number" id="labelSize" name="labelSize" min="2" max="46" step="2" value="10">

    <label for="stickyToggle">Sticky Layout</label>
    <input type="checkBox" id="stickyToggle" name="stickyToggle">

</body>

<div class="modal fade" id="saveSettingsMenu" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Save Settings</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <label for="saveSettingsName">Name:</label>
        <input type="text" name="saveSettingsName" id="saveSettingsName">

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="saveSettingsButton">Save</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<div class="modal" id="loadSettingsSelector" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Load Settings</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        
        <div class="list-group">
            <?php foreach(getSettingsFromDB($loggedInUser) as $settings) { ?>
                <button id="singleSettingBtn" type="button" class="list-group-item list-group-item-action"><?=
                $settings["settings_id"] ?></button>
            <?php } ?>
        </div>

      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="loadSettingsButton">Load</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
    

</html>