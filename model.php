<?php
// File:        model.php
// Author:      Hank Feild
// Updated by:  YOUR NAME HERE
// Date:        26-Sep-2018
// Purpose:     Handles the data for the Todo List app.


// Information about the database. Usually you'd keep this in a file in a
// non-web-accessible directory on the server. That way, if PHP goes down
// for some reason and the actual PHP code is exposed, the database
// details (which often include credentials) are not exposed.

// $dbName = "/home/hfeild/csc302-fa18-data/lecture06-todo.db";
$dbName = "/home/tamello/csc302-fa18-data/lecture06-todo1.db";
// FIX ^^
$dsn = "sqlite:$dbName";
$dbh = null;


function error($errorMessage){
    die(json_encode([
        "success" => false,
        "error" => $errorMessage
    ]));
}


/**
 * Attempts to make a connection the database if one doesn't already exist, 
 * failing otherwise. Uses the $dsn global and sets the $dbh global.
 */
function connectToDB(){
    global $dsn;
    global $dbh;

    try{
        if($dbh == null){
            $dbh = new PDO($dsn);
            setupDB();
        }
    } catch(PDOException $e) {
        die("Couldn't establish a connection to the database: ". 
            $e->getMessage());
    }
}

/**
 * Sets up the database, creating the users and todo_items tables if they're 
 * not already there.
 */
function setupDB(){
    global $dbh;
    try {
        // Set up the users table.
        $dbh->exec("create table if not exists users(".
            "id integer primary key autoincrement,".
            "username text,".
            "password text,".
            "auth_token text)");

        // Check for errors.
        $error = $dbh->errorInfo();
        if($error[0] !== '00000' && $error[0] !== '01000') {
            die("There was an error setting up the users table: ". $error[2]);
        }

        // Setup the todo_items table.
        $dbh->exec("create table if not exists settings(".
            "id integer primary key autoincrement,".
            "user_id integer,".
            "settings_id text,".
            "gravity text,".
            "link_distance text,".
            "charge text,".
            "node_size text,".
            "label_size text)");

        // Check for errors.
        $error = $dbh->errorInfo();
        if($error[0] !== '00000' && $error[0] !== '01000') {
            die("There was an error setting up the users table: ". $error[2]);
        }
    } catch(PDOException $e) {
        die("There was an error setting up the database: ". $e->getMessage());
    }
}

/**
 * Saves the data for the given username. 
 * 
 * @param username The username.
 * @param password The password hash for the user.
 */
function addNewUser($username, $password){
    global $dbh;
    connectToDB();

    try {
        $statement = $dbh->prepare("insert into users(username, password) ".
            "values(:username, :password)");

        $success = $statement->execute(array(
            ":username" => $username, 
            ":password" => $password));
           
        if(!$success){
            die("There was an error saving to the database: ". 
                $dbh->errorInfo()[2]);
        }
    } catch(PDOException $e){
        die("There was an error saving to the database: ". $e->getMessage());
    }
}

/**
 * Retrieves data for the given username. 
 * 
 * @param username The username to lookup.
 * @return The data associated with the given username, or null if the username
 *         doesn't exist. The returned data is an associative array with these
 *         fields:
 *           - id
 *           - username
 *           - password
 *           - auth_token
 */
function getUserInfo($username){
    global $dbh;
    connectToDB();

    try {
        $statement = $dbh->prepare(
            "select * from users where username = :username");
        $success = $statement->execute(array(
            ":username" => $username));
           
        if(!$success){
            die("There was an error reading from the database: ". 
                $dbh->errorInfo()[2]);
        } else {
            return $statement->fetch(PDO::FETCH_ASSOC);
        }
    } catch(PDOException $e){
        die("There was an error reading from the database: ". $e->getMessage());
    }
}

/**
 * Retrieves data for the given auth token. 
 * 
 * @param auth_token The auth token to look the user up by.
 * @return The data associated with the given username, or null if the username
 *         doesn't exist. The returned data is an associative array with these
 *         fields:
 *           - id
 *           - username
 *           - password
 *           - auth_token
 */
function getUserInfoByAuthToken($auth_token){
    global $dbh;
    connectToDB();

    try {
        $statement = $dbh->prepare(
            "select * from users where auth_token = :auth_token");
        $success = $statement->execute(array(
            ":auth_token" => $auth_token));
           
        if(!$success){
            die("There was an error reading from the database: ". 
                $dbh->errorInfo()[2]);
        } else {
            return $statement->fetch(PDO::FETCH_ASSOC);
        }
    } catch(PDOException $e){
        die("There was an error reading from the database: ". $e->getMessage());
    }
}

/**
 * Retrieves data for the given auth token. 
 * 
 * @param userId The id of the user.
 * @param authToken The auth token to assign the user.
 */
function setUserAuthToken($userId, $authToken){
    global $dbh;
    connectToDB();

    try {
        $statement = $dbh->prepare(
            "update users set auth_token = :auth_token ".
                "where id = :user_id");
        $success = $statement->execute(array(
            ":user_id"    => $userId,
            ":auth_token" => $authToken));
           
        if(!$success){
            die("There was an error updating the database: ". 
                $dbh->errorInfo()[2]);
        } 
    } catch(PDOException $e){
        die("There was an error updating the database: ". $e->getMessage());
    }
}

function addSettingsToDB($userId, $settingsId, $gravity, $linkDistance, $charge, $nodeSize, $labelSize){
    global $dbh;
    connectToDB();

    try {
        $statement = $dbh->prepare("INSERT INTO settings(user_id, settings_id, gravity, link_distance, charge, node_size, label_size) VALUES(:user_id, :settings_id, :gravity, :link_distance, :charge, :node_size, :label_size)");

        $success = $statement->execute(array(
            ":user_id" => $userId,
            ":settings_id" => $settingsId,
            ":gravity" => $gravity,
            ":link_distance" => $linkDistance,
            ":charge" => $charge,
            ":node_size" => $nodeSize,
            ":label_size" => $labelSize));

        if (!$success) {
            die("There was an error saving to the database: ".
                  $dbh->errorInfo()[2]);
        }

        
    } catch (PDOException $e) {
        die("There was an error saving to the database: ". $e->getMessage());
    }
}

function saveSettingsToDB($userId, $settingsId, $gravity, $linkDistance, $charge, $nodeSize, $labelSize){
    global $dbh;
    connectToDB();

    try {
        $statement = $dbh->prepare("UPDATE settings SET gravity = :gravity, link_distance = :link_disance, charge = :charge, node_size = :node_size, label_size = :label_size WHERE settings_id = :settings_id AND user_id = :user_id");

        $success = $statement->execute(array(
            ":user_id" => $userId,
            ":settings_id" => $settingsId,
            ":gravity" => $gravity,
            ":link_distance" => $linkDistance,
            ":charge" => $charge,
            ":node_size" => $nodeSize,
            ":label_size" => $labelSize));

        if (!$success) {
            die("There was an error saving to the database: ".
                  $dbh->errorInfo()[2]);
        }

        
    } catch (PDOException $e) {
        die("There was an error saving to the database: ". $e->getMessage());
    }
}

function getSettingsFromDB($userId) {
    global $dbh;
    connectToDB();

    try {
        $statement = $dbh->prepare("SELECT * FROM settings WHERE user_id = :user_id");

        $success = $statement->execute(array(
            ":user_id" => $userId));

        if (!$success) {
            die("There was an error reading from the database: ".
                  $dbh->errorInfo()[2]);
        } else {
            $thisSettings = $statement->fetchAll(PDO::FETCH_ASSOC);
            return $thisSettings;
        }
    } catch (PDOException $e) {
        die("THere was an error reading from the database: ".
              $e->getMessage());
    }


}

function getOneSettingFromDB($userId, $settingsId) {
    global $dbh;
    connectToDB();

    try {
        $statement = $dbh->prepare("SELECT gravity,link_distance,charge,node_size,label_size FROM settings WHERE user_id = :user_id AND settings_id = :settings_id");

        $success = $statement->execute(array(
            ":user_id" => $userId,
            ":settings_id" => $settingsId));

        if (!$success) {
            die("There was an error reading from the database: ".
                  $dbh->errorInfo()[2]);
        } else {
            $thisSettings = $statement->fetchAll(PDO::FETCH_ASSOC)[0];
            echo $thisSettings["gravity"]." ".$thisSettings["link_distance"]." ".$thisSettings["charge"]." ".$thisSettings["node_size"]." ".$thisSettings["label_size"];
        }
    } catch (PDOException $e) {
        die("THere was an error reading from the database: ".
              $e->getMessage());
    }
}

/**
 * Validates that the given username is associated with the todo list item.
 * 
 * @param username The username of the user changing the id.
 * @param todoId The id of the todo list item.
 * @return True if username is owner of the todo, false otherwise.
 */
function validateUserHasGraphPermissions($username, $todoId){
    global $dbh;
    connectToDB();

    try {
        $statement = $dbh->prepare(
            "select count(*) from users as u ".
                "join todo_items as t ".
                "where u.username = :username and ".
                "u.id = t.user_id and t.id = :todo_id");
        $success = $statement->execute(array(
            ":username" => $username,
            ":todo_id" => $todoId));
           
        if(!$success){
            die("There was an error reading from the database: ". 
                $dbh->errorInfo()[2]);
        } else {
            return $statement->fetchColumn() > 0;
        }

    } catch(PDOException $e){
        die("There was an error reading from the database: ". $e->getMessage());
    }
}

?>
