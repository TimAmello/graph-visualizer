# Graph Visualizer

This is a web-based csv/tsv visualizer. A user can upload a csv or tsv file that 
has the edges of a dataset and the program
will make an adjustable, animated graph from it. 
